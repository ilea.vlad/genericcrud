package com.example.demo.abstractClass;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

public abstract class GenericController<T,N> {

    private final GenericService<T,N> genericService;

    public GenericController(GenericService<T, N> genericService) {
        this.genericService = genericService;
    }

    @PostMapping()
    public ResponseEntity<T> createStudent(@RequestBody T entity) {
        return ResponseEntity.ok(genericService.create(entity));
    }

    @GetMapping
    public ResponseEntity<List<T>> getAllStudents() {
        List<T> entities = genericService.getAll();
        if (entities.isEmpty()) {
            return ResponseEntity.noContent().build();
        } else {
            return ResponseEntity.ok(entities);
        }

    }

    @GetMapping("/{id}")
    public ResponseEntity<T> getStudentById(@PathVariable(name = "id") N id) {
        Optional<T> enity = genericService.get(id);
        if (enity.isPresent()) {
            return ResponseEntity.ok(enity.get());
        } else {
            return ResponseEntity.noContent().build();
        }
    }


    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteStudent(@PathVariable(name = "id") N id) {
        genericService.delete(id);
        return ResponseEntity.noContent().build();
    }

}
