package com.example.demo.controller;

import com.example.demo.abstractClass.GenericController;
import com.example.demo.abstractClass.GenericService;
import com.example.demo.entity.Domain;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/domain")
public class DomainController extends GenericController<Domain,Long> {


    public DomainController(GenericService<Domain, Long> genericService) {
        super(genericService);
    }
}
