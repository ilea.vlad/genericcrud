package com.example.demo.controller;

import com.example.demo.abstractClass.GenericController;
import com.example.demo.abstractClass.GenericService;
import com.example.demo.entity.Student;
import com.example.demo.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/student")
public class StudentController extends GenericController<Student, Long> {

    private final StudentService studentService;
    @Autowired
    public StudentController(@Qualifier("studentServiceImpl") GenericService<Student, Long> genericService, StudentService studentService) {
        super(genericService);
        this.studentService = studentService;
    }

    @GetMapping("/getByAge/{age}")
    public ResponseEntity<Student> getByAge(@PathVariable(name = "age") Integer age){
        return ResponseEntity.ok(studentService.findByAge(age));
    }
}
