package com.example.demo.repository;

import com.example.demo.abstractClass.GenericRepo;
import com.example.demo.entity.Domain;
import org.springframework.stereotype.Repository;

@Repository
public interface DomainRepository extends GenericRepo<Domain,Long> {
}
