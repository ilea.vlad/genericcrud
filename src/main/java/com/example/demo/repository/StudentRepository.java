package com.example.demo.repository;

import com.example.demo.abstractClass.GenericRepo;
import com.example.demo.entity.Student;
import org.springframework.stereotype.Repository;

@Repository
//QueryDSL
public interface StudentRepository extends GenericRepo<Student,Long> {

    Student findByAge(Integer age);
}
