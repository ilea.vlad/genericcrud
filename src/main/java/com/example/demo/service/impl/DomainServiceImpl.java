package com.example.demo.service.impl;

import com.example.demo.abstractClass.GenericRepo;
import com.example.demo.abstractClass.GenericServiceImpl;
import com.example.demo.entity.Domain;
import org.springframework.stereotype.Service;

@Service
public class DomainServiceImpl extends GenericServiceImpl<Domain,Long> {
    public DomainServiceImpl(GenericRepo<Domain, Long> genericRepo) {
        super(genericRepo);
    }
}
