package com.example.demo.service.impl;

import com.example.demo.abstractClass.GenericRepo;
import com.example.demo.abstractClass.GenericServiceImpl;
import com.example.demo.entity.Student;
import com.example.demo.repository.StudentRepository;
import com.example.demo.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StudentServiceImpl extends GenericServiceImpl<Student,Long> implements StudentService {
    private final StudentRepository studentRepository;

    @Autowired
    public StudentServiceImpl(GenericRepo<Student, Long> genericRepo,StudentRepository studentRepository) {
        super(genericRepo);
        this.studentRepository = studentRepository;
    }


    @Override
    public Student findByAge(Integer age) {
        return studentRepository.findByAge(age);
    }
}
